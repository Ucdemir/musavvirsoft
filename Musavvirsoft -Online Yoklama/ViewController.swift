//
//  ViewController.swift
//  Musavvirsoft -Online Yoklama
//
//  Created by Omer Demirci on 11.06.2020.
//  Copyright © 2020 Kermesten. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, UIScrollViewDelegate,WKNavigationDelegate,WKUIDelegate {
    
    
    
    @IBOutlet weak var proggressBar: UIProgressView!
    @IBOutlet weak var containerView: UIView!
    
    var webView  = WKWebView()
    
    var isOpenCall = false
    
    override var prefersStatusBarHidden: Bool {
       return true
     }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        proggressBar.progress = 0
                   webView = WKWebView(frame: containerView.bounds)
                    containerView.autoresizesSubviews = true
                    webView.autoresizingMask = ([.flexibleWidth, .flexibleHeight])
                    containerView.addSubview(webView)
                    
                    webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
               webView.customUserAgent = "Mozilla/5.0 (Linux; Android 5.1.1; SM-N750K Build/LMY47X; ko-kr) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Mobile Safari/537.36 Puffin/6.0.8.15804AP"
         
           
               
               webView.configuration.preferences.javaScriptCanOpenWindowsAutomatically = true
                webView.configuration.preferences.javaScriptEnabled = true
                //webView.configuration.preferences.plugInsEnabled = true
               webView.isMultipleTouchEnabled = false
               if #available(iOS 10.0, *) {
                   webView.configuration.dataDetectorTypes = .phoneNumber
               } else {
                   
               }

               webView.load(URLRequest(url: URL(string: "https://online-yoklama.musavvirsoft.com/giris.php")!))

               webView.scrollView.delegate = self
               webView.navigationDelegate = self
               webView.uiDelegate = self
    }

     func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
             scrollView.pinchGestureRecognizer?.isEnabled = false
        }
        
        override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
             if keyPath == "estimatedProgress" {
                 proggressBar.progress = Float(webView.estimatedProgress)
                //Zoom disabled
                
             let meta = """
    var meta = document.createElement('meta');
    meta.name = "viewport";
    meta.content = "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no";
    document.getElementsByTagName('head')[0].appendChild(meta);
    """
                webView.evaluateJavaScript(meta, completionHandler: nil)

                
                
                
    


             }
            
           
            
             
         }
         deinit {
             if isViewLoaded {
                 webView.removeObserver(self, forKeyPath: NSStringFromSelector(#selector(getter: WKWebView.estimatedProgress)))
             }
             // if you have set either WKWebView delegate also set these to nil here
             webView.navigationDelegate = nil
             webView.uiDelegate = nil
         }
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
           
           guard let url = navigationAction.request.url else {return}
           
           if navigationAction.request.url?.scheme == "tel" {
               
               callNumber(number: url.absoluteURL.absoluteString)
               
             
              }
           
           
    
           
           if navigationAction.navigationType == .linkActivated {
              
               webView.load(URLRequest(url: url))
           }
           decisionHandler(.allow)
       }
       
       
     

       

       
       func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
           let alertController = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)

           alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK button"), style: .default, handler: { (action) in
               completionHandler()
           }))

           present(alertController, animated: true, completion: nil)
       }

       func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
           let alertController = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)

           alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK button"), style: .default, handler: { (action) in
               completionHandler(true)
           }))

           alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel button"), style: .default, handler: { (action) in
               completionHandler(false)
           }))

           present(alertController, animated: true, completion: nil)
       }

       func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
           let alertController = UIAlertController(title: nil, message: prompt, preferredStyle: .actionSheet)

           alertController.addTextField { (textField) in
               textField.text = defaultText
           }

           alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK button"), style: .default, handler: { (action) in
               if let text = alertController.textFields?.first?.text {
                   completionHandler(text)
               } else {
                   completionHandler(defaultText)
               }
           }))

           alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel button"), style: .default, handler: { (action) in
               completionHandler(nil)
           }))

           present(alertController, animated: true, completion: nil)
       }
       
       
       /*func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
           if (request.url?.scheme == "doneEditing") {
               // Call you event handler here
               //doneButtonTouchEventHandler()
               
               print(1)
               return false
           }
           return true
       }*/

       
       
       func hideBottomBar(){
        //   viewBottomBar.isHidden = true;
       }
       
    
    func callNumber(number : String){
        
        
        
        if let url = URL(string: "\(number)"), UIApplication.shared.canOpenURL(url) {
                  if #available(iOS 10, *) {
                    if(isOpenCall){
                    
                    UIApplication.shared.open(url,options: [:],completionHandler: {
                        (succes)in
                        
                        self.isOpenCall = false
                    })
                  }
                  
                  }
                  else {
                    UIApplication.shared.openURL(url)
                  }
              }
        
        isOpenCall = true
    }

}

